#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#define __sys_pause std::system("pause")
#define __sys_clear std::system("cls")
#else
#define __sys_pause                                                                                                    \
    std::cout << "Press any key to continue . . ." << std::endl;                                                       \
    std::cin.get();
#define __sys_clear std::system("clear")
#endif

#include <boost/log/trivial.hpp>
#include <boost/mpi.hpp>

#ifndef __SC_PLATFORM_HPP__
#define __SC_PLATFORM_HPP__

namespace sc {
    /**
     * @brief Function that prints banner into the log.
     * @param message Message to be displayed in the middle of banner.
     * @param comm MPI Communicator.
     */
    void banner(std::string message, boost::mpi::communicator comm);

    /**
     * @brief Method used to run function with banner and MPI Barrier synchronization.
     * @param function Function that is run inside.
     * @param comm MPI Communicator.
     * @param banner_message Message to be displayed in the middle of banner.
     */
    void run_task(
        void (*function)(boost::mpi::communicator c), boost::mpi::communicator comm, std::string banner_message);

    /**
     * @brief Method used to run function with banner and MPI Barrier synchronization.
     * @param function Function that is run inside.
     * @param comm MPI Communicator.
     * @param arg Second argument passed to function.
     * @param banner_message Message to be displayed in the middle of banner.
     */
    void run_task(void (*function)(boost::mpi::communicator c, int i), boost::mpi::communicator comm, int arg,
        std::string banner_message);
}

#endif