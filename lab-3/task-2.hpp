#ifndef __SC_TASK_2_HPP__
#define __SC_TASK_2_HPP__

#include <boost/mpi.hpp>
#include <boost/log/trivial.hpp>
#include <boost/format.hpp>
#include "timer.hpp"

namespace sc {
    /**
     * @brief Function finding Primes among all numbers up until limit using MPI multithreading.
     * @param comm MPI Communicator.
     * @param limit Final number to be checked.
    */
    void findPrimes(boost::mpi::communicator comm, int limit);
}
#endif // !__SC_TASK_2_HPP__
