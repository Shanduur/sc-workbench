#include "platform.hpp"

namespace sc {
    void banner(std::string message, boost::mpi::communicator comm) {
        if (comm.rank() == 0) {
            BOOST_LOG_TRIVIAL(info) << "-----------------------------------------------";
            BOOST_LOG_TRIVIAL(info) << "";
            BOOST_LOG_TRIVIAL(info) << message;
            BOOST_LOG_TRIVIAL(info) << "";
            BOOST_LOG_TRIVIAL(info) << "-----------------------------------------------";
        }
    }

    void run_task(
        void (*function)(boost::mpi::communicator c), boost::mpi::communicator comm, std::string banner_message) {
        banner(banner_message, comm);
        comm.barrier();
        function(comm);
        comm.barrier();
    }

    void run_task(void (*function)(boost::mpi::communicator c, int i), boost::mpi::communicator comm, int arg,
        std::string banner_message) {
        banner(banner_message, comm);
        comm.barrier();
        function(comm, arg);
        comm.barrier();
    }
}
