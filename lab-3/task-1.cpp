#include "task-1.hpp"

namespace sc {
    void oneToOne(boost::mpi::communicator comm) {
        int rank = comm.rank();
        int size = comm.size();

        // just to ensure all task are initialized
        comm.barrier();

        if (size % 2 != 0) {
            BOOST_LOG_TRIVIAL(error) << "not enough processes";
            return;
        }

        if (rank % 2) {
            std::string message;
            comm.recv(rank - 1, 0, message);
            BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] message recieved: %2%") % rank % message;
        }
        else {
            std::string message = boost::str(boost::format("hello from %1%") % rank);
            comm.send(rank + 1, 0, message);
            BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] message sent") % rank;
        }
    }

    void oneToMany(boost::mpi::communicator comm) {
        int rank = comm.rank();
        int size = comm.size();

        // just to ensure all task are initialized
        comm.barrier();

        if (size < 2) {
            BOOST_LOG_TRIVIAL(error) << "not enough processes";
            return;
        }

        if (rank == 0) {
            for (int i = 1; i < size; i++) {
                std::string message = boost::str(boost::format("ECHO %1%") % i);
                comm.send(i, 0, message);
                BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] message sent: %2%") % rank % message;
            }

            for (int i = 1; i < size; i++) {
                std::string message;
                comm.recv(boost::mpi::any_source, 0, message);
                BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] message recieved: %2%") % rank % message;
            }
        }
        else {
            std::string message;
            auto status = comm.recv(boost::mpi::any_source, 0, message);
            message = boost::str(boost::format("%1% OK") % message);
            comm.send(status.source(), 0, message);
        }
    }

    void nonBlockingWait(boost::mpi::communicator comm) {
        int rank = comm.rank();
        int size = comm.size();

        // just to ensure all task are initialized
        comm.barrier();

        if (size % 2 != 0) {
            BOOST_LOG_TRIVIAL(error) << "not enough processes";
            return;
        }

        boost::mpi::request reqs[2];
        std::string message_2, message_1;

        if (rank % 2) {
            message_1 = boost::str(boost::format("hello to %1% from %2%") % (rank - 1) % rank);
            reqs[0] = comm.isend(rank - 1, 0, message_1);
            reqs[1] = comm.irecv(rank - 1, 0, message_2);
        }
        else {
            message_1 = boost::str(boost::format("hello to %1% from %2%") % (rank + 1) % rank);
            reqs[0] = comm.isend(rank + 1, 0, message_1);
            reqs[1] = comm.irecv(rank + 1, 0, message_2);
        }

        boost::mpi::wait_all(reqs, reqs + 2);
        BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] message recieved: %2%") % rank % message_2;
    }

    void nonBlockingTest(boost::mpi::communicator comm) {
        int rank = comm.rank();
        int size = comm.size();

        // just to ensure all task are initialized
        comm.barrier();

        if (size % 2 != 0) {
            BOOST_LOG_TRIVIAL(error) << "not enough processes";
            return;
        }

        std::vector<boost::mpi::request> reqs;
        std::string message_2, message_1;

        if (rank % 2) {
            message_1 = boost::str(boost::format("hello to %1% from %2%") % (rank - 1) % rank);
            reqs.push_back(comm.isend(rank - 1, 0, message_1));
            reqs.push_back(comm.irecv(rank - 1, 0, message_2));
        }
        else {
            message_1 = boost::str(boost::format("hello to %1% from %2%") % (rank + 1) % rank);
            reqs.push_back(comm.isend(rank + 1, 0, message_1));
            reqs.push_back(comm.irecv(rank + 1, 0, message_2));
        }

        std::vector<bool> finished = { false, false };
        while (!finished[0] || !finished[1]) {
            for (int i = 0; i < reqs.size(); i++) {
                auto status_opt = reqs[i].test();
                if (status_opt.has_value()) {
                    finished[i] = true;
                }
                else {
                    BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] waiting...") % rank;
                }
            }
        }

        BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] message recieved: %2%") % rank % message_2;
    }

    void oneToManyBarrier(boost::mpi::communicator comm) {
        int rank = comm.rank();
        int size = comm.size();

        // just to ensure all task are initialized
        comm.barrier();

        if (size < 2) {
            BOOST_LOG_TRIVIAL(error) << "not enough processes";
            return;
        }

        if (rank == 0) {
            for (int i = 1; i < size; i++) {
                std::string message = boost::str(boost::format("AT %1%") % i);
                comm.send(i, 0, message);
                BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] sent message: %2%") % rank % message;
            }
            comm.barrier();
            BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] all sent") % rank;
        }
        else {
            std::string message;
            auto status = comm.recv(boost::mpi::any_source, 0, message);
            comm.barrier();
            BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] message recieved: %2%") % rank % message;
        }
    }
}