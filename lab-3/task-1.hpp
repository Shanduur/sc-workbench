#ifndef __SC_TASK_1_HPP__
#define __SC_TASK_1_HPP__

#include <boost/mpi.hpp>
#include <boost/log/trivial.hpp>
#include <boost/format.hpp>
#include <vector>

namespace sc {
    /**
     * @brief Function testing o2o communication in MPI.
     * @param comm MPI Communicator.
    */
    void oneToOne(boost::mpi::communicator comm);

    /**
     * @brief Function testing o2m communication in MPI.
     * @param comm MPI Communicator.
    */
    void oneToMany(boost::mpi::communicator comm);

    /**
     * @brief Function testing non-blocking communication in MPI (wait_all).
     * @param comm MPI Communicator.
    */
    void nonBlockingWait(boost::mpi::communicator comm);

    /**
     * @brief Function testing non-blocking communication in MPI (test).
     * @param comm MPI Communicator.
     */
    void nonBlockingTest(boost::mpi::communicator comm);

    /**
     * @brief Function testing o2m communication in MPI (with additional Barrier).
     * @param comm MPI Communicator.
    */
    void oneToManyBarrier(boost::mpi::communicator comm);
}

#endif // !__SC_TASK_1_HPP__
