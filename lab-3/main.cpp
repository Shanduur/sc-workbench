#include <boost/mpi.hpp>

#include "platform.hpp"
#include "task-1.hpp"
#include "task-2.hpp"

namespace mpi = boost::mpi;

auto main(int argc, char* argv[]) -> int {
    mpi::environment env;
    mpi::communicator world;

    sc::run_task(sc::oneToOne, world, "One to One");

    sc::run_task(sc::oneToMany, world, "One to Many");

    sc::run_task(sc::nonBlockingWait, world, "NonBlocking with Wait");

    sc::run_task(sc::nonBlockingTest, world, "NonBlocking with Test");

    sc::run_task(sc::oneToManyBarrier, world, "One to Many with Barrier");

    sc::run_task(sc::findPrimes, world, 1e7, "Finding primes");

    return 0;
}
