#include "task-2.hpp"

namespace sc {
    /**
     * @brief Simple function for checking if the number is prime.
     * @param num Number to be checked.
     * @return Returns true if number is prime.
    */
    bool isPrime(int num) {
        if (num < 2)
            return false;
        for (int i = 2; i <= sqrt(int(num)); i++)
            if (num % i == 0)
                return false;
        return true;
    }

    /**
     * @brief Simple struct holding begining and end of the chunk.
    */
    typedef struct t_chunk {
        int start;
        int end;
    };

    /**
     * @brief Helper function dividing range into the chunks.
     * @param start Defines starting point of the range.
     * @param end Defines ending point of the range.
     * @param num_chunks Number of chunks into which the range will be divided.
     * @return Returns vector holding structs with indications of start and end of array.
    */
    std::vector<t_chunk> makeChunks(int start, int end, int num_chunks) {
        std::vector<struct t_chunk> sizes;

        int num_chunks_1 = num_chunks, num_chunks_2 = 0;
        int size_1 = (end - start) / num_chunks_1;
        int size_2 = 0;

        while ((size_1 * num_chunks_1 + size_2 * num_chunks_2) != end - start) {
            if (size_2 == 0)
                size_2 = ((size_1 * num_chunks_1 + size_2 * num_chunks_2) < end - start) ? size_1 + 1 : size_1 - 1;

            --num_chunks_1;
            ++num_chunks_2;
        }

        int total = 0;
        for (int i = 0; i < num_chunks_1; i++) {
            sizes.push_back(t_chunk{ total, total + size_1 });
            total = total + size_1;
        }

        for (int i = 0; i < num_chunks_2; i++) {
            sizes.push_back(t_chunk{ total, total + size_2 });
            total = total + size_2;
        }

        return sizes;
    }

    void findPrimes(boost::mpi::communicator comm, int limit) {
        int rank = comm.rank();
        int size = comm.size();
        timer::PerfCounter<timer::SystemTimer>* t = nullptr;
        if (rank == 0)
            t = new timer::PerfCounter<timer::SystemTimer>();

        int worker_result = 0;
        t_chunk chunk;

        if (rank == 0) {
            auto chunks = makeChunks(0, limit, size);

            for (int i = 1; i < size; i++) {
                comm.send(i, 0, chunks[i].start);
                comm.send(i, 1, chunks[i].end);
            }

            chunk.start = chunks[0].start;
            chunk.end = chunks[0].end;
        }
        else {
            comm.recv(0, 0, chunk.start);
            comm.recv(0, 1, chunk.end);
        }

        BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] chunk size: %2% chunk start: %3%, chunk end: %4%") % rank %
                                        (chunk.end - chunk.start) % chunk.start % chunk.end;

        for (int i = chunk.start; i < chunk.end; i++) {
            if (isPrime(i))
                worker_result++;
        }

        int total_result = 0;
        boost::mpi::reduce(comm, worker_result, total_result, std::plus<int>(), 0);

        if (rank == 0) {
            BOOST_LOG_TRIVIAL(trace) << boost::format("[%1%] primes found: %2%") % rank % total_result;
            if (t != nullptr)
                delete t;
        }
    }
}