#define ARRSIZE 10000
#define MAX_CONSUMERS 256
#define TASKSIZE 500  /* 500 */
#define QUEUESIZE 100 /* 100 */

#define SLEEP_TIME 0

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#define __sys__pause std::system("pause")
#define __sys__clear std::system("cls")
#include <Windows.h>
#else
#define __sys__pause                                                                                                   \
    std::cout << "Press any key to continue . . ." << std::endl;                                                       \
    std::cin.get();
#define __sys__clear std::system("clear")
#include <unistd.h>
#endif

#define TRACE_MM(x1, x2)                                                                                               \
    BOOST_LOG_TRIVIAL(debug) << boost::format("[%1%] %2%:%3%: %4% | %5%") % m_comm.rank() % __FILE__ % __LINE__ % x1 % \
                                    x2;                                                                                \
    Sleep(SLEEP_TIME)

#define TRACE_M(x1) TRACE_MM(x1, "")

#define TRACE TRACE_M("")