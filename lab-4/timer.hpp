#ifndef __SC_TIMER_HPP__
#define __SC_TIMER_HPP__

#include <chrono>
#include <ctime>
#include <optional>
#include <boost/log/trivial.hpp>
#include <boost/mpi.hpp>

namespace timer {
    /**
     * @brief Simple class that logs it's lifetime to std::out with [TIME] tag.
     * @tparam T one of Timer classes declared in this header.
     */
    template <typename T> class PerfCounter {
    public:
        PerfCounter() = default;
        ~PerfCounter();

    private:
        T m_timer;
    };

    template <typename T> PerfCounter<T>::~PerfCounter() { BOOST_LOG_TRIVIAL(debug) << m_timer.sinceStart(); }

    /**
     * @brief timer class using std::steady_clock.
     */
    class SteadyTimer {
    public:
        SteadyTimer();
        std::string sinceStart();
        void print();

    private:
        std::chrono::steady_clock::time_point m_start;
    };

    /**
     * @brief timer class using std::system_clock.
     */
    class SystemTimer {
    public:
        SystemTimer();
        std::string sinceStart();
        void print();

    private:
        std::chrono::system_clock::time_point m_start;
    };

    /**
     * @brief timer class using std::time.
     */
    class CTimer {
    public:
        CTimer();
        std::string sinceStart();
        void print();

    private:
        std::time_t m_start;
    };
}

#endif // !__SC_TIMER_HPP__