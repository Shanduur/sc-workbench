#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include <atomic>
#include <mutex>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <boost/mpi.hpp>
#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>

#include "timer.hpp"
#include "messages.hpp"
#include "config.hpp"

namespace sc {
    /**
     * @brief Queue class that works in MPI environment.
     * @tparam T Template type, stored inide the queue.
     */
    template <typename T> class Queue {
    public:
        /**
         * @brief Main constructor of the Queue.
         * @param max_size Maximal capacity of the Queue.
         */
        Queue(const unsigned long long max_size, boost::mpi::communicator comm)
            : m_maxSize(max_size), m_comm(comm), m_producerDoneReq(), m_sendReqs() {
            m_pingReq_consumer = m_comm.irecv(boost::mpi::any_source, Tags::ping);
            m_pingReq_producer = m_comm.isend(1, Tags::request);
            m_producerDoneReq = m_comm.irecv(1, Tags::end);
        };
        Queue() = delete;
        ~Queue() {
            int size = m_comm.size();
            std::vector<boost::mpi::request> reqs;
            for (int i = 2; i < size; i++) {
                reqs.push_back(m_comm.isend(i, Tags::end));
            }
        };

        /**
         * @brief Pop method is used to retrieve data from the Queue.
         * @return T variable, if pop was succesful, otherwise optional without value.
         */
        auto pop() -> boost::optional<T> {
            if (m_length > 0) {
                T tmp = m_queue.front();
                m_queue.pop();
                m_length--;
                return tmp;
            }
            else
                return {};
        }

        /**
         * @brief This method is used to insert data into the queue.
         * @param data Data that will be pushed to the queue.
         * @return Returns true on succesfull push, otherwise returns false.
         */
        auto push(T data) -> bool {
            if (m_length < m_maxSize) {
                m_length++;
                m_queue.push(data);
                return true;
            }
            else
                return false;
        }


        /**
         * @brief This method is main method for running the queue.
         * @param num_consumers Number of consumer processes.
         */
        auto Run() -> void {
            bool ask_producer = true;
            bool producer_done = false;
            bool sent = false;
            bool ps_had_value = false;
            bool popped_had_value = false;
            bool rs_had_value = false;
            int id;

            for (;;) {
                if (m_producerDoneReq.test()) {
                    producer_done = true;
                }
                
                if (producer_done && m_queue.empty()) {
                    break;
                }

                auto ps = m_pingReq_consumer.test();
                if (ps.has_value()) {
                    id = ps.value().source();
                    ps_had_value = true;
                    auto val = pop();
                    if (val.has_value()) {
                        req_push(id, val.value());
                        m_pingReq_consumer = m_comm.irecv(boost::mpi::any_source, Tags::ping);
                    }
                    else {
                        sent = false; 
                    }
                }
                
                if (ps_had_value && !sent) {
                    auto val = pop();
                    if (val.has_value()) {
                        req_push(id, val.value());
                        sent = true;
                        m_pingReq_consumer = m_comm.irecv(boost::mpi::any_source, Tags::ping);
                    }
                }

                auto rs = m_pingReq_producer.test();
                if (rs.has_value() ) {
                    rs_had_value = true;
                }

                if (rs_had_value && ask_producer) {
                    m_recvReq = m_comm.irecv(1, Tags::transfer, m_recieved);
                    ask_producer = false;
                }

                auto popped = req_pop();
                if (popped.has_value()) {
                    popped_had_value = true;
                    T arr = popped.value();
                    bool ok = push(arr);
                    if (ok) {
                        ask_producer = true;
                        popped_had_value = false;
                        m_pingReq_producer = m_comm.isend(1, Tags::request);
                    }
                }

                if (popped_had_value) {
                    bool ok = push(m_recieved);
                    if (ok) {
                        ask_producer = true;
                        popped_had_value = false;
                        m_pingReq_producer = m_comm.isend(1, Tags::request);
                    }
                }

                for (auto it = m_sendReqs.begin(); it != m_sendReqs.end();) {
                    if (it->test())
                        it = m_sendReqs.erase(it);
                    else
                        it++;
                }
            }

            while (!m_sendReqs.empty()) {
                for (auto it = m_sendReqs.begin(); it != m_sendReqs.end();) {
                    if (it->test())
                        it = m_sendReqs.erase(it);
                    else
                        it++;
                }
            }
        }

    private:
        std::queue<T> m_queue;
        unsigned long long m_length;
        boost::mpi::communicator m_comm;
        unsigned long long m_maxSize;
        T m_recieved;

        boost::mpi::request m_producerDoneReq;
        boost::mpi::request m_pingReq_consumer;
        boost::mpi::request m_pingReq_producer;
        boost::mpi::request m_recvReq;
        std::vector<boost::mpi::request> m_sendReqs;

        /**
         * @brief Method used to retrieve data from the producer.
         * @return Returns T if request was tested succesfully, otherwise std::optional with no value.
         */
        auto req_pop() -> std::optional<T> {
            if (m_recvReq.test()) {
                return m_recieved;
            }

            return {};
        }

        /**
         * @brief Method sending payload from the queue to the consumer process.
         * @param id ID of the consumer.
         * @param val Payload that will be sent.
         */
        auto req_push(int id, T val) -> void {
            m_sendReqs.push_back(m_comm.isend(id, Tags::transfer, val));
        }
    };
}

#endif // !__QUEUE_HPP__
