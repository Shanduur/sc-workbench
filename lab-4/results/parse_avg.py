import os

def Average(lst):
    return sum(lst) / len(lst)

for fname in os.listdir("."):
    if fname.endswith(".log"):
        times = list()
        fhandle = open(fname, 'r')
        lines = fhandle.readlines()
        for l in lines:
            if "debug" not in l:
                continue
            else:
                s = l.split()
                i = int(s[4])
                times.append(i)
        if "Release" in fname:
            print(f"release,{Average(times)}")
        else:
            print(f"debug,{Average(times)}")
