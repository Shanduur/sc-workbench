#ifndef __SC_PRODUCER_HPP__
#define __SC_PRODUCER_HPP__

#include "config.hpp"
#include "messages.hpp"

namespace sc {
    /**
     * @brief Producer class performing generation of data for queue.
     * @tparam T Teplate type, works only with iterable classes.
     */
    template <typename T> class Producer {
    public:
        Producer(boost::mpi::communicator comm) : m_comm(comm), m_sendReqs(), m_produced(0) {
            m_pingReq = m_comm.irecv(boost::mpi::any_source, Tags::request);
        };
        Producer() = delete;
        ~Producer() {
            auto reqs = m_comm.isend(0, Tags::end);
            reqs.wait();
        };

        /**
         * @brief Main method of producer.
         * @param iterations Number of items to be generated.
         * @param generator Function pointer with generator method.
         */
        auto Run(const unsigned int iterations, T (*generator)()) -> void {
            for (;;) {
                auto s = m_pingReq.test();
                if (s.has_value()) {
                    push(s.value().source(), generator);
                    m_pingReq = m_comm.irecv(boost::mpi::any_source, Tags::request);
                }

                for (auto it = m_sendReqs.begin(); it != m_sendReqs.end();) {
                    if (it->test())
                        it = m_sendReqs.erase(it);
                    else
                        it++;
                }

                if (m_produced == iterations)
                    break;
            }

            while (!m_sendReqs.empty()) {
                for (auto it = m_sendReqs.begin(); it != m_sendReqs.end();) {
                    if (it->test())
                        it = m_sendReqs.erase(it);
                    else
                        it++;
                }
            }
        }

    private:
        boost::mpi::communicator m_comm;
        unsigned int m_produced;
        boost::mpi::request m_pingReq;
        std::vector<boost::mpi::request> m_sendReqs;

        /**
         * @brief Method sending generated payload to the queue process.
         * @param generator Function pointer with generator method.
         */
        auto push(int id, T (*generator)()) -> void {
            T val = generator();

            m_sendReqs.push_back(m_comm.isend(id, Tags::transfer, val));
            m_produced++;
        }
    };
}

#endif // !__SC_PRODUCER_HPP__
