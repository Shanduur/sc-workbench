#include <boost/log/trivial.hpp>
#include <boost/format.hpp>
#include <boost/mpi.hpp>
#include <string>

#include "consumer.hpp"
#include "producer.hpp"
#include "queue.hpp"
#include "timer.hpp"
#include "config.hpp"

namespace mpi = boost::mpi;

static unsigned long x = 123456789, y = 362436069, z = 521288629;

/**
 * @brief XOR Shift pseudo random number generator.
 * @return Returns pseudo random unsigned long.
 */
unsigned long xorshf96() {
    unsigned long t;
    x ^= x << 16;
    x ^= x >> 5;
    x ^= x << 1;

    t = x;
    x = y;
    y = z;
    z = t ^ x ^ y;

    return z;
}

void run_consumer(mpi::communicator &comm) {
    sc::Consumer<std::array<int, ARRSIZE>> consumer(comm);
    consumer.Run();
}

void run_producer(mpi::communicator &comm) {
    sc::Producer<std::array<int, ARRSIZE>> producer(comm);

    auto generator = []() {
        std::array<int, ARRSIZE> arr;
        for (size_t i = 0; i < ARRSIZE; i++) {
            arr[i] = (int)xorshf96();
        }

        return arr;
    };

    producer.Run(TASKSIZE, generator);
}

void run_queue(mpi::communicator &comm) {
    int size = comm.size();
    sc::Queue<std::array<int, ARRSIZE>> queue(QUEUESIZE, comm);

    queue.Run();
}


auto main() -> int {
    timer::PerfCounter<timer::SteadyTimer> t;
    mpi::environment env;
    mpi::communicator world;

    auto rank = world.rank();
    auto size = world.size();

    if (size < 2) {
        BOOST_LOG_TRIVIAL(error) << "rank too low, restart application with higher number of workers";
        exit(1);
    }

    if (rank == 0)
        run_queue(world);
    else if (rank == 1)
        run_producer(world);
    else
        run_consumer(world);

    return 0;
}