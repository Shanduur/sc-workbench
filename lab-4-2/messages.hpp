#ifndef __SC_MESSAGES_HPP__
#define __SC_MESSAGES_HPP__

namespace sc {
    /**
     * @brief Enum containing Tags that can be sent in the MPI messages.
    */
    typedef enum Tags {
        ping,
        request,
        transfer,
        response,
        end,
    };
}

#endif // !__SC_MESSAGES_HPP__
