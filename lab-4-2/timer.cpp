#include "timer.hpp"

namespace timer {

    SteadyTimer::SteadyTimer() { m_start = std::chrono::steady_clock::now(); }

    std::string SteadyTimer::sinceStart() {
        return std::to_string((std::chrono::steady_clock::now() - m_start).count());
    }

    void SteadyTimer::print() { BOOST_LOG_TRIVIAL(debug) << sinceStart(); }

    SystemTimer::SystemTimer() { m_start = std::chrono::system_clock::now(); }

    std::string SystemTimer::sinceStart() {
        return std::to_string((std::chrono::system_clock::now() - m_start).count());
    }

    void SystemTimer::print() { BOOST_LOG_TRIVIAL(debug) << sinceStart(); }

    CTimer::CTimer() { m_start = std::time(nullptr); }

    std::string CTimer::sinceStart() { return std::to_string(std::time(nullptr) - m_start); }

    void CTimer::print() { BOOST_LOG_TRIVIAL(debug) << sinceStart(); }
}