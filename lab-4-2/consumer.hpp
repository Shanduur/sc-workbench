#ifndef __SC_CONSUMER_HPP__
#define __SC_CONSUMER_HPP__

#include <boost/mpi.hpp>
#include <boost/log/trivial.hpp>
#include <optional>
#include <algorithm>

#include "config.hpp"
#include "messages.hpp"

namespace sc {
    /**
     * @brief Consumer class performing consumption of resources from queue.
     * @tparam T Teplate type, works only with iterable classes.
    */
    template <typename T> class Consumer {
    public:
        Consumer(boost::mpi::communicator comm) : m_comm(comm), m_sorted(0), m_recieved(), m_recvReq() {
            m_producerDoneReq = m_comm.irecv(0, Tags::end);
            m_pingReq = m_comm.isend(0, Tags::ping);
        };
        Consumer() = delete;
        ~Consumer() {
            BOOST_LOG_TRIVIAL(debug) << boost::format("[%1%] sorted %2%") % m_comm.rank() % m_sorted;
        };

        /**
         * @brief Main method of consumer.
        */
        auto Run() -> void {
            

            while (true) {
                if (m_producerDoneReq.test()) {
                    break;
                }

                if (m_pingReq.test()) {
                    m_recvReq = m_comm.irecv(0, Tags::transfer, m_recieved);
                }

                auto popped = pop();

                if (popped.has_value()) {
                    T arr = popped.value();
                    std::sort(arr.begin(), arr.end());
                    m_sorted++;
                    auto cksm = checksum(arr);
                    m_pingReq = m_comm.isend(0, Tags::ping);
                }
            }
        }

    private:
        boost::mpi::communicator m_comm;
        boost::mpi::request m_pingReq;
        boost::mpi::request m_recvReq;
        boost::mpi::request m_producerDoneReq;
        unsigned int m_sorted;
        T m_recieved;


        /**
         * @brief Method used to retrieve data from the queue.
         * @param req MPI Request.
         * @return Returns T if request was tested succesfully, otherwise std::optional with no value.
        */
        auto pop() -> std::optional<T> {
            if (m_recvReq.test()) {
                return m_recieved;
            }

            return {};
        }

        /**
         * @brief Simple method for calculating sum of all array items.
         * @param arr Array from which all items are summed.
         * @return Sum of all items.
        */
        int checksum(const T &arr) {
            int c = 0;
            for (int i : arr) {
                c += i;
            }
            return c;
        }
    };
}

#endif // !__SC_CONSUMER_HPP__
