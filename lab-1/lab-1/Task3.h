#ifndef __TASK_3_H__
#define __TASK_3_H__

#include <atomic>
#include <chrono>
#include <iostream>
#include <vector>
#include <mutex>

/**
 * @brief class with increment methods and benchmarking capability
*/
class Task3 {
private:
    std::mutex m_mutex;
    std::atomic<unsigned long> m_avar = 0;
    unsigned long m_mvar = 0;
    unsigned long m_uvar = 0;

public:
    /**
     * @brief benchmarking methods executng all functions specified in the Task 3
     * @param increments number of increments to be performed during functions benchmarking inside constructor
     * @param num_threads number of threads for execution
    */
    void benchmark(unsigned long increments, unsigned int num_threads);

    /**
     * @brief increment of the member variable using thread safe mutex
     * @param increments number of increments to be performed by single method run
    */
    void mutex_increment(unsigned int increments);

    /**
     * @brief increment of the member variable using thread safe atomic variable
     * @param increments number of increments to be performed by single method run
    */
    void atomic_increment(unsigned int increments);

    /**
     * @brief thread unsafe increment of the member variable
     * @param increments number of increments to be performed by single method run
    */
    void unsafe_increment(unsigned int increments);
};

/**
 * @brief class used for time benchmarking code scopes
*/
class Timer {
private:
    std::chrono::high_resolution_clock::time_point m_start;

public:
    /**
     * @brief default constructor - assignes time point of the creation to private variable
    */
    Timer();

    /**
     * @brief default destructor - displays time of existence of the class
    */
    ~Timer();
};

#endif
