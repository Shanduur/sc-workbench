#include "Task1.h"

Task1::Task1() {
    std::string payload = "thread payload";
    auto* mut = new std::mutex();

    auto functor = cObj(1, mut);
    auto* ct = new cFunct(2, mut);

    auto t1 = std::thread(functor, payload);
    auto t2 = std::thread(&cFunct::print, ct, payload);
    auto t3 = std::thread(gFunct_print, mut, 3, payload);
    auto t4 = std::thread(
        [&payload, mut](int id) {
            mut->lock();
            std::cout << "printed from lambda function: id(" << id << ") payload("
                      << payload << ")" << std::endl;
            mut->unlock();
        },
        4);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
}

cObj::cObj(int id, std::mutex* mutex) : m_id(id), m_mut(mutex) {}

void cObj::operator()(std::string str) {
    std::cout << "printed from functor: id(" << m_id << ") payload(" << str << ")"
              << std::endl;
}

cFunct::cFunct(unsigned int id, std::mutex* mutex) : m_id(id), m_mut(mutex) {}

void cFunct::print(std::string str) {
    m_mut->lock();
    std::cout << "printed from class function: id(" << m_id << ") payload(" << str
              << ")" << std::endl;
    m_mut->unlock();
}

void gFunct_print(std::mutex* mutex, int id, std::string str) {
    mutex->lock();
    std::cout << "printed from global function: id(" << id << ") payload(" << str
              << ")" << std::endl;
    mutex->unlock();
}