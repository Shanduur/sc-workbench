#ifndef __TASK_1_H__
#define __TASK_1_H__

#include <iostream>
#include <mutex>
#include <thread>

/**
 * @brief class executng all necessary functions from Task 1 in the constructor
*/
class Task1 {
public:
    Task1();
};

/**
 * @brief function object / functor used for displaying message
*/
class cObj {
private:
    int m_id;
    std::mutex* m_mut;

public:
    /**
     * @brief default constructor for the class, assumes that it will be instantiated once for the thread
     * @param id of the thread
     * @param mutex mutex object for assuring the correct order of stdout access
    */
    cObj(int id, std::mutex* mutex);

    /**
     * @brief () operator overloading for using the class as functor
     * @param str message string
    */
    void operator()(std::string str);
};

/**
 * @brief class with public method printing string
*/
class cFunct {
private:
    int m_id;
    std::mutex* m_mut;

public:
    /**
     * @brief default constructor for the class, assumes that it will be instantiated once for the thread
     * @param id of the thread
     * @param mutex mutex object for assuring the correct order of stdout access
    */
    cFunct(unsigned int id, std::mutex* mutex);

    /**
     * @brief internal method for printing the given string and additional info
     * @param str message string
    */
    void print(std::string str);
};

/**
 * @brief global function for printing thread ID and stirng
 * @param mutex object for assuring the correct order of stdout access
 * @param id number of thread
 * @param str message string
*/
void gFunct_print(std::mutex* mutex, int id, std::string str);

#endif
