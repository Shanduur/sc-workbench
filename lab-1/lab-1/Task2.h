#ifndef __TASK_2_H__
#define __TASK_2_H__

#include <iostream>
#include <list>
#include <mutex>
#include <thread>

/**
 * @brief class executng all necessary functions from Task 1 in the constructor
 */
class Task2 {
public:
    /**
     * @brief default constructor for the class
     * @param num_threads number of threads to be executed inside the constructor
    */
    Task2(unsigned int num_threads);
};


/**
 * @brief 
 * @param iterations 
 * @param thread_id 
 * @param msg message string
*/
void gPrint(int iterations, unsigned int thread_id, std::string msg);

#endif
