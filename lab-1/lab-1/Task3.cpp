#include "Task3.h"

void Task3::benchmark(unsigned long increments, unsigned int num_threads) {
    auto threads = std::vector<std::thread>();
    unsigned long target = increments * num_threads;

    std::cout << "Single Execution" << std::endl << "MUTEX" << std::endl;
    {
        Timer t;
        mutex_increment(increments);
    }
    std::cout << "ATOMIC" << std::endl;
    {
        Timer t;
        atomic_increment(increments);
    }
    std::cout << "UNSAFE" << std::endl;
    {
        Timer t;
        unsafe_increment(increments);
    }

    m_mvar = 0;
    m_avar = 0;
    m_uvar = 0;

    std::cout << std::endl
              << "Parallel Execution for " << num_threads << " threads and " << increments << " increments" << std::endl
              << "MUTEX" << std::endl;
    {
        Timer t;
        for (unsigned int i = 0; i < num_threads; i++) {
            threads.push_back(std::thread(&Task3::mutex_increment, this, increments));
        }

        for (auto& th : threads)
            th.join();
    }

    threads.clear();
    std::cout << "ATOMIC" << std::endl;
    {
        Timer t;
        for (unsigned int i = 0; i < num_threads; i++) {
            threads.push_back(std::thread(&Task3::atomic_increment, this, increments));
        }

        for (auto& th : threads)
            th.join();
    }

    threads.clear();
    std::cout << "UNSAFE" << std::endl;
    {
        Timer t;
        for (unsigned int i = 0; i < num_threads; i++) {
            threads.push_back(std::thread(&Task3::unsafe_increment, this, increments));
        }

        for (auto& th : threads)
            th.join();
    }

    std::cout << std::endl
              << "method:\tvalue\t\ttarget\t\ttarget met" << std::endl
              << "MUTEX :\t" << m_mvar << "  \t" << target << "\t" << (m_mvar == target) << "\t" << std::endl
              << "ATOMIC:\t" << m_avar << "  \t" << target << "\t" << (m_avar == target) << "\t" << std::endl
              << "UNSAFE:\t" << m_uvar << "  \t" << target << "\t" << (m_uvar == target) << "\t" << std::endl;
}

void Task3::mutex_increment(unsigned int increments) {
    for (size_t i = 0; i < increments; i++) {
        std::lock_guard<std::mutex> guard(m_mutex);
        m_mvar++;
    }
}

void Task3::atomic_increment(unsigned int increments) {
    for (size_t i = 0; i < increments; i++) {
        m_avar.fetch_add(1, std::memory_order_acq_rel);
    }
}

void Task3::unsafe_increment(unsigned int increments) {
    for (size_t i = 0; i < increments; i++) {
        m_uvar++;
    }
}

Timer::Timer() { m_start = std::chrono::high_resolution_clock::now(); }

Timer::~Timer() { std::cout << "Time: " << (std::chrono::high_resolution_clock::now() - m_start).count() << std::endl; }
