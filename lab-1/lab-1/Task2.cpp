#include "Task2.h"

Task2::Task2(unsigned int num_threads) {
    std::cout << "Threads: " << num_threads << std::endl;
    auto threads = std::list<std::thread>();

    for (unsigned int i = 0; i < num_threads; i++)
        threads.push_back(std::thread(gPrint, 50, i, "payload"));

    for (auto& th : threads)
        th.join();
}

std::mutex s_printMutex;
void gPrint(int iterations, unsigned int thread_id, std::string msg) {
    for (int i = 0; i < iterations; i++) {
        std::lock_guard<std::mutex> guard(s_printMutex);
        std::cout << thread_id << ": " << msg << std::endl;
    }
}