#include "Task1.h"
#include "Task2.h"
#include "Task3.h"

#include <iostream>
#include <cmath>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#define pause std::system("pause")
#define clear std::system("cls")
#else
#define pause                                                                                                          \
    std::cout << "Press any key to continue . . ." << std::endl;                                                          \
    std::cin.get();
#define clear std::system("clear")
#endif

auto main() -> int {
    std::cout << "Task 1:" << std::endl;
    Task1();

    pause;
    clear;

    std::cout << "Task 2:" << std::endl;
    Task2(20);

    pause;
    clear;

    std::cout << "Task 3:" << std::endl;
    Task3 t3;
    t3.benchmark(1e6, 10);

    pause;
}
