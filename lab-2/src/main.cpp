﻿#include <array>
#include <atomic>
#include <chrono>
#include <iostream>
#include <list>
#include <sstream>
#include <vector>
#include <thread>
#include <random>

#include "config.hpp"
#include "consumer.hpp"
#include "producer.hpp"
#include "queue.hpp"
#include "timer.hpp"
#include "logging.hpp"

static unsigned long x = 123456789, y = 362436069, z = 521288629;

/**
 * @brief
 * @param
 * @return
 */
unsigned long xorshf96(void) {
    unsigned long t;
    x ^= x << 16;
    x ^= x >> 5;
    x ^= x << 1;

    t = x;
    x = y;
    y = z;
    z = t ^ x ^ y;

    return z;
}

int main() {
    timer::PerfCounter<timer::CTimer> t;

    auto log = Log::GetInstance();

    std::list<int> test_threads;
    for (int i = 1; i <= 24; i++) {
        test_threads.push_back(i);
    }
    test_threads.push_back(32);
    test_threads.push_back(48);
    test_threads.push_back(96);

    std::list<std::string> test_times;
    std::list<std::string>* thread_info = new std::list<std::string>();
    sc::ConsumersInfo* cinfo = new sc::ConsumersInfo();


    for (auto thread_count : test_threads) {
        timer::SteadyTimer t;
        std::vector<std::thread> threads;

        auto queue = new sc::Queue<std::array<int, ARRSIZE>>(QUEUESIZE);
        sc::Producer prod(queue);
        sc::Consumer cons(queue);

        auto generator = [](int size) {
            std::array<int, ARRSIZE> arr;
            for (size_t i = 0; i < ARRSIZE; i++) {
                arr[i] = (int)xorshf96();
            }

            return arr;
        };

        auto queue_thread = [](sc::Queue<std::array<int, ARRSIZE>>* q) {
            auto log = Log::GetInstance();
            while (!q->isDone()) {}
            log->Debugln("Producer Done");
            while (!q->isEmpty()) {}
            log->Debugln("Queue Empty");
            while (q->consumers()) {}
            log->Debugln("Queue Consumers Done");
        };

        threads.push_back(std::thread(queue_thread, queue));
        threads.push_back(std::thread(prod, TASKSIZE, generator));

        for (int i = 0; i < thread_count; i++) {
            threads.push_back(std::thread(cons, cinfo));
        }

        for (auto& th : threads) {
            th.join();
        }
        auto tp = t.sinceStart();

        test_times.push_back(tp);

        log->Statln("Threads: " + std::to_string(thread_count));
        log->Timeln(tp);

        if (!queue->isEmpty())
            throw new std::exception("queue not empty");

        threads.clear();
        delete queue;
    }

    if (test_threads.size() == test_times.size()) {
        auto size = test_times.size();
        auto info = cinfo->info();

        log->CSV('w', "thread count", "time taken");
        for (int i = 0; i < size; i++) {
            auto time = test_times.front();
            test_times.pop_front();

            auto threads = test_threads.front();
            test_threads.pop_front();

            log->Statln(std::string("thread count: " + std::to_string(threads) + "\ttime taken: " + time));
            
            log->CSV(threads, time);
            std::stringstream ss;
            for (int i = 0; i < threads; i++) {
                ss << info.front() << " ";
                info.pop_front();
            }
            log->Statln("sorted by threads: " + ss.str());
        }
    }
    else {
        log->Warnln("sizes not equal!");
    }

    return 0;
}
