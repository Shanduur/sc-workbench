#include "consumer.hpp"

namespace sc {
    Consumer::Consumer(Queue<std::array<int, ARRSIZE>>* queue) { m_queue = queue; }

    void Consumer::operator()(ConsumersInfo* consumers_info) {
        m_queue->addConsumer();
        int i = 0;
        Log* log = Log::GetInstance();

        while (!m_queue->isDone() || !m_queue->isEmpty()) {
            std::optional<std::array<int, ARRSIZE>> popped = m_queue->pop();
            if (popped.has_value()) {
                std::array<int, ARRSIZE> arr = popped.value();
                std::sort(arr.begin(), arr.end());
                auto cksm = checksum(arr);
                //log->Println("checksum: " + std::to_string());
                i++;
            }
            else {
                //log->Debugln("no value in the queue");
                std::this_thread::yield();
            }
        }

        if (!m_queue->isEmpty())
            throw std::exception("consumer finished prematurly");

        log->Println("sorted: " + std::to_string(i));
        consumers_info->add(std::to_string(i));
        m_queue->removeConsumer();
    }

    void ConsumersInfo::add(std::string info) {
        std::lock_guard<std::mutex> lg(m_mutex);
        m_info.push_back(info);
    }

    std::list<std::string> ConsumersInfo::info() { return m_info; }

    int checksum(std::array<int, ARRSIZE> arr) {
        int c = 0;
        for (int i : arr) {
            c += i;
        }
        return c;
    }
}