#include "producer.hpp"

namespace sc {
    Producer::Producer(Queue<std::array<int, ARRSIZE>>* queue) { m_queue = queue; }

    void Producer::operator()(int iterations, std::array<int, ARRSIZE> (*generator)(int size)) {
        int i = 0;
        Log* log = Log::GetInstance();
        while (i < iterations) {
            std::array<int, ARRSIZE> val = generator(m_queue->size());
            if (m_queue->push(val)) {
                i++;
            }
            else {
                //log->Debugln("unable to push into the queue");
                std::this_thread::yield();
            }
        }
        m_queue->done();
    }
}