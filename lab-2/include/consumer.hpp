#ifndef __CONSUMER_HPP__
#define __CONSUMER_HPP__

#include <array>
#include <algorithm>
#include <list>
#include <sstream>
#include <typeinfo>
#include <thread>

#include "queue.hpp"
#include "config.hpp"
#include "logging.hpp"

namespace sc {
    /**
     * @brief Class holding consumers information / statistics.
     */
    class ConsumersInfo {
    public:
        ConsumersInfo() = default;
        ~ConsumersInfo() = default;

        /**
         * @brief method used for inserting information into the list of information.
         * @param info information string to be inserted into information list.
         */
        void add(std::string info);

        /**
         * @brief method used for retrieving the list of information.
         * @return list of information.
         */
        std::list<std::string> info();

    private:
        std::list<std::string> m_info;
        std::mutex m_mutex;
    };

    /**
     * @brief implementation of Consumer (Producer/Consumer design pattern).
    */
    class Consumer {
    public:
        /**
         * @brief constructor takes pointer to sc::Queue as parameter.
         * @param queue thread safe Queue.
         */
        Consumer(Queue<std::array<int, ARRSIZE>>* queue);
        Consumer() = delete;
        ~Consumer() = default;

        /**
         * @brief operator() overloading for using the class as functor.
         * @param consumers_info pointer to class holding consumers information / statistics.
        */
        void operator()(ConsumersInfo* consumers_info);

    private:
        Queue<std::array<int, ARRSIZE>>* m_queue;
    };

    /**
     * @brief function for calculating checksum of all members of the array.
     * @param arr array from which the checksum is calculated.
     * @return checksum.
     */
    int checksum(std::array<int, ARRSIZE> arr);
}

#endif // !__CONSUMER_HPP__
