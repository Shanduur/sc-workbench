#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include <atomic>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <list>

#include "logging.hpp"

namespace sc {
    /**
     * @brief
     * @tparam T
     */
    template <typename T> class Queue {
    public:
        /**
         * @brief
         * @param max_size
         */
        Queue(const unsigned long long max_size);
        Queue() = delete;
        ~Queue() = default;

        /**
         * @brief
         * @return
         */
        bool isEmpty();

        /**
         * @brief
         * @return
         */
        bool isFull();

        /**
         * @brief
         * @return
         */
        std::optional<T> pop();

        /**
         * @brief
         * @param data
         * @return
         */
        bool push(T data);

        /**
         * @brief
         * @return
         */
        unsigned long long size();

        /**
         * @brief
         */
        void done();

        /**
         * @brief
         * @return
         */
        bool isDone();

        /**
         * @brief
         */
        void addConsumer();

        /**
         * @brief
         */
        void removeConsumer();

        /**
         * @brief
         * @return
         */
        int consumers();

    private:
        std::queue<T> m_queue;
        std::mutex m_mutex;
        std::atomic<unsigned long long> m_length;
        std::list<int> m_consumers;
        bool m_producerDone;
        unsigned long long m_maxSize;
    };

    template <typename T>
    Queue<T>::Queue(const unsigned long long max_size) : m_maxSize(max_size), m_producerDone(false){};

    template <typename T> bool Queue<T>::isEmpty() { return (m_length.load() == 0); }

    template <typename T> bool Queue<T>::isFull() { return (m_length.load() == m_maxSize); }

    template <typename T> unsigned long long Queue<T>::size() { return m_maxSize; }

    template <typename T> std::optional<T> Queue<T>::pop() {
        std::lock_guard<std::mutex> lg(m_mutex);
        if (m_length > 0) {
            T tmp = m_queue.front();
            m_queue.pop();
            m_length--;
            return tmp;
        }
        else
            return {};
    }

    template <typename T> bool Queue<T>::push(T data) {
        std::lock_guard<std::mutex> lg(m_mutex);
        if (m_length < m_maxSize) {
            m_length++;
            m_queue.push(data);
            return true;
        }
        else
            return false;
    }

    template <typename T> void Queue<T>::done() {
        std::lock_guard<std::mutex> lg(m_mutex);
        m_producerDone = true;
    }

    template <typename T> bool Queue<T>::isDone() { return m_producerDone; }

    template <typename T> void Queue<T>::addConsumer() {
        std::lock_guard<std::mutex> lg(m_mutex);
        m_consumers.push_back(1);
    }

    template <typename T> void Queue<T>::removeConsumer() {
        std::lock_guard<std::mutex> lg(m_mutex);
        m_consumers.pop_back();
    }

    template <typename T> int Queue<T>::consumers() {
        std::lock_guard<std::mutex> lg(m_mutex);
        auto log = Log::GetInstance();
        return !m_consumers.empty();
    }
}

#endif // !__QUEUE_HPP__
