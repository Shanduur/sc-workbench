#ifndef __PRODUCER_HPP__
#define __PRODUCER_HPP__

#include <array>
#include <typeinfo>
#include <thread>

#include "queue.hpp"
#include "config.hpp"
#include "logging.hpp"

namespace sc {
    /**
     * @brief implementation of Producer (Producer/Consumer design pattern).
    */
    class Producer {
    public:
        /**
         * @brief constructor takes pointer to sc::Queue as parameter.
         * @param queue thread safe Queue.
        */
        Producer(Queue<std::array<int, ARRSIZE>>* queue);
        Producer() = delete;
        ~Producer() = default;

        /**
         * @brief operator() overloading for using the class as functor.
         * @param iterations number of queue elements to generate.
         * @param generator function pointer for element generator.
        */
        void operator()(int iterations, std::array<int, ARRSIZE> (*generator)(int size));

    private:
        Queue<std::array<int, ARRSIZE>>* m_queue;
    };
}

#endif // !__PRODUCER_HPP__
