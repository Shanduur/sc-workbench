#ifndef __LOGGING_HPP__
#define __LOGGING_HPP__

#include <string>
#include <mutex>
#include <iostream>
#include <fstream>

#include "config.hpp"

/**
 * @brief Simple thread safe singleton logging class.
*/
class Log {
protected:
    Log() = default;
    static inline Log* m_singleton;

private:
    std::fstream file;
    std::mutex m_mutex;
    std::mutex m_fileMutex;

public:
    Log(Log& other) = delete;
    void operator=(const Log&) = delete;

    /**
     * @brief singleton function returning instance of class.
     * @return instance of the Log class.
    */
    static inline Log* GetInstance() {
        if (m_singleton == nullptr) {
            m_singleton = new Log();
        }
        return m_singleton;
    }

    /**
     * @brief Method printing message with [INFO] tag.
     * @param message message that should be displayed.
    */
    void Println(std::string message) {
        std::lock_guard<std::mutex> lg(m_mutex);
        std::cout << "[INFO] " << message << std::endl;
    }

    /**
     * @brief Method printing message with [DEBG] tag. It will display anything only if __DEBUG__ is defined.
     * @param message message that should be displayed.
    */
    void Debugln(std::string message) {
        std::lock_guard<std::mutex> lg(m_mutex);
#ifdef __DEBUG__
            std::cout << "[DEBG] " << message << std::endl;
#endif
    }

    /**
     * @brief Method printing message with [ERRO] tag.
     * @param message message that should be displayed.
    */
    void Errorln(std::string message) {
        std::lock_guard<std::mutex> lg(m_mutex);
        std::cout << "[ERRO] " << message << std::endl;
    }

    /**
     * @brief Method printing message with [WARN] tag.
     * @param message message that should be displayed.
    */
    void Warnln(std::string message) {
        std::lock_guard<std::mutex> lg(m_mutex);
        std::cout << "[WARN] " << message << std::endl;
    }

    /**
     * @brief Method printing message with [TIME] tag.
     * @param message message that should be displayed.
    */
    void Timeln(std::string message) {
        std::lock_guard<std::mutex> lg(m_mutex);
        std::cout << "[TIME] " << message << std::endl;
    }

    /**
     * @brief Method printing message with [STAT] tag.
     * @param message message that should be displayed.
    */
    void Statln(std::string message) {
        std::lock_guard<std::mutex> lg(m_mutex);
        std::cout << "[STAT] " << message << std::endl;
    }

    /**
     * @brief function writing single two column line to the CSV file.
     * @param c1 variable that will be put in column 1.
     * @param c2 variable that will be put in column 2.
    */
    void CSV(int c1, std::string c2) {
        std::lock_guard<std::mutex> lg(m_fileMutex);
        file.open("stats.csv", std::ios::out | std::ios::app);
        file << c1 << "," << c2 << std::endl;
        if (file.is_open()) {
            file.close();
        }
    }

    /**
     * @brief function writing single two column line to the CSV file.
     * @param mode file opening mode, 'a' for appending, 'w' for overwriting.
     * @param c1 variable that will be put in column 1.
     * @param c2 variable that will be put in column 2.
    */
    void CSV(char mode, std::string c1, std::string c2) { 
        std::lock_guard<std::mutex> lg(m_fileMutex);
        if (mode == 'a') {
            file.open("stats.csv", std::ios::out | std::ios::app);
            file << c1 << "," << c2 << std::endl;
        }
        else if (mode == 'w') {
            file.open("stats.csv", std::ios::out);
            file << c1 << "," << c2 << std::endl;
        }
        if (file.is_open()) {
            file.close();
        }
    }
};

#endif // !__LOGGING_HPP__
