#define ARRSIZE 10000
#define TASKSIZE 500
#define QUEUESIZE 100

#define __DEBUG__ false

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#define __sys__pause std::system("pause")
#define __sys__clear std::system("cls")
#else
#define __pause                                                                                                          \
    std::cout << "Press any key to continue . . ." << std::endl;                                                       \
    std::cin.get();
#define __sys__clear std::system("clear")
#endif
